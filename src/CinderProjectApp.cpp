#include <list>
#include <glm/gtc/random.hpp>

#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Particle {
public:
	Particle(vec2 position)
	{
		m_position = position;
		m_direction = glm::circularRand(100.0f);
		m_velocity = glm::linearRand(0.2f, 0.5f);
		m_radius = glm::linearRand(1.0f, 5.0f);
	}

	void update()
	{
		m_position += m_direction * m_velocity;
	}

	void draw()
	{
		gl::drawSolidCircle(m_position, m_radius);
	}
private:
	vec2 m_position;
	vec2 m_direction;
	float m_velocity;
	float m_radius;
};

class ParticleController {
public:
	ParticleController() {}
	void update() {
		for (list<Particle>::iterator particle = m_particles.begin(); particle != m_particles.end(); ++particle) {
			particle->update();
		}
	}
	void draw() {
		for (list<Particle>::iterator particle = m_particles.begin(); particle != m_particles.end(); ++particle) {
			particle->draw();
		}
	}
	void addParticles(vec2 position, int amount) {
		for (int i = 0; i < amount; i++) {
			m_particles.push_back(Particle(position));
		}
	}
	void removeParticles(int amount) {
		if (amount < m_particles.size())
		{
			for (int i = 0; i < amount; i++) {
				m_particles.pop_front();
			}
		}
	}
private:
	list<Particle> m_particles;
};

class CinderProjectApp : public App {
public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
	ParticleController particle_controller;
};


void CinderProjectApp::setup()
{
	this->setFullScreen(true);
}

void CinderProjectApp::mouseDown( MouseEvent event )
{
	particle_controller.addParticles(event.getPos(),10);
}

void CinderProjectApp::update()
{
	particle_controller.addParticles(glm::circularRand(800.0f), glm::linearRand(15, 25));
	particle_controller.update();
}

void CinderProjectApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
	particle_controller.draw();
	particle_controller.removeParticles(glm::linearRand(15, 1000));
}

CINDER_APP( CinderProjectApp, RendererGl )
